module Codec.GlTF.Buffer
  ( BufferIx(..)
  , Buffer(..)
  ) where

import Codec.GlTF.Prelude

import Codec.GlTF.URI (URI)

newtype BufferIx = BufferIx { unBufferIx :: Int }
  deriving (Eq, Ord, Show, FromJSON, ToJSON, Generic)

-- | A buffer points to binary geometry, animation, or skins.
--
-- glTF Buffer referring to GLB-stored BIN chunk, must have @buffer.uri@
-- property undefined, and it must be the first element of buffers array;
-- byte length of BIN chunk could be up to 3 bytes bigger than JSON-defined
-- buffer.byteLength to satisfy GLB padding requirements.
data Buffer = Buffer
  { byteLength :: Size
  , uri        :: Maybe URI
  , name       :: Maybe Text
  , extensions :: Maybe Object
  , extras     :: Maybe Value
  } deriving (Eq, Show, Generic)

instance FromJSON Buffer
instance ToJSON Buffer
