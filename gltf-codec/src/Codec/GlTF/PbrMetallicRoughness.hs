module Codec.GlTF.PbrMetallicRoughness
  ( PbrMetallicRoughness(..)
  ) where

import Codec.GlTF.Prelude

import Codec.GlTF.TextureInfo (TextureInfo_)

-- | A set of parameter values that are used to define the metallic-roughness
-- material model from Physically-Based Rendering (PBR) methodology.
data PbrMetallicRoughness = PbrMetallicRoughness
  { baseColorFactor          :: (Float, Float, Float, Float)
  , metallicFactor           :: Float
  , roughnessFactor          :: Float
  , metallicRoughnessTexture :: Maybe TextureInfo_
  , baseColorTexture         :: Maybe TextureInfo_
  , extensions               :: Maybe Object
  , extras                   :: Maybe Value
  } deriving (Eq, Show, Generic)

instance FromJSON PbrMetallicRoughness where
  parseJSON = withObject "PbrMetallicRoughness" \o -> do
    baseColorFactor          <- o .:? "baseColorFactor" .!= (1.0, 1.0, 1.0, 1.0)
    metallicFactor           <- o .:? "metallicFactor"  .!= 1.0
    roughnessFactor          <- o .:? "roughnessFactor" .!= 1.0

    metallicRoughnessTexture <- o .:? "metallicRoughnessTexture"
    baseColorTexture         <- o .:? "baseColorTexture"

    extensions               <- o .:? "extensions"
    extras                   <- o .:? "extras"
    pure PbrMetallicRoughness{..}

instance ToJSON PbrMetallicRoughness
