-- XXX: Misplaced here due to mutual recursion.

module Codec.GlTF.Skin
  ( SkinIx(..)
  , Skin(..)
  ) where

import Codec.GlTF.Node (SkinIx(..), Skin(..))
